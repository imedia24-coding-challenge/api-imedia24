## PS:

1- This project does not contain all the best practices in coding, we can discuss them in pair programming interview (because I am not sure if this Coding Exercise is still valid)

2- I used Java not Kotlin because I have not used Kotlin before but I do not have a problem switching to it.

3- For the coding challenge for the frontend, to be honest, I have not used  React, I am familiar with Angular, if the tech lead agrees to do the exercise with Angular, I can do, else, l need some days to learn React to do the challenge.


Swagger UI:

http://localhost:8080/swagger-ui/index.html

## Docker
to use docker:

1- Install Docker by following the steps from https://www.docker.com/

2- open terminal in the root directory where exists Dockerfile of this project

create network and database:

```docker network create imedia24_network```

``docker run -it -d --restart "unless-stopped" -e MYSQL_ROOT_PASSWORD="root1234" -e MYSQL_PASSWORD="pass1234" -e MYSQL_USER="imedia24" -e MYSQL_DATABASE="shop" --name imedia_db --network imedia24_network mysql:8.0.27``

run this command to build the artifact:

```mvn clean package  -Dmaven.test.skip=true```

run this command to build docker image:

```docker build --tag api_imedia24:v2 --build-arg="springProfilesActive=staging" .```

run this command to run the docker image:

```docker run -d -p 8080:8080 --name api_imedia24 --network imedia24_network api_imedia24:v2 --rm```

baseUrl: http://localhost:8080/api/v1/

endPoint products: {baseUrl}/products

![ci-cd.png](ci-cd.png)