# there are different method to dockerlize java app:
# - with Spring boot plugin or Jib plugin google
# - or manually

# our base build image
FROM eclipse-temurin:21_35-jre-alpine
ARG springProfilesActive
ENV springProfilesActive_env=${springProfilesActive}
ENV artifact_name="api-imedia.jar"
LABEL org.opencontainers.image.authors="anbara"

COPY target/${artifact_name} ${artifact_name}

EXPOSE 8080

ENTRYPOINT ["java","-Dspring.profiles.active=${springProfilesActive_env}","-jar","/api-imedia.jar"]
