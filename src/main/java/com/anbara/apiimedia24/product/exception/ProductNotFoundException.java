package com.anbara.apiimedia24.product.exception;

/**
 * @author Ayoub ANBARA
 */
public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(String message) {
        super(message);
    }
}
