package com.anbara.apiimedia24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiImedia24Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiImedia24Application.class, args);
	}

}
